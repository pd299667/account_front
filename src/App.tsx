import TodoListStateKeeper from 'comp/TodoEditor/state/TodoListStateKeeper';
import { Provider } from 'mobx-react';
import React from 'react';
import MainPage from './page/MainPage';

function App() {

  return (
    <Provider todoListStateKeeper = {TodoListStateKeeper.instance}>

      <div className="App">
        <MainPage />
      </div>
    </Provider>
  );
}

export default App;
