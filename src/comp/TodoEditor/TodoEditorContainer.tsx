import React from 'react';
import { inject, observer } from 'mobx-react';
import autobind from 'autobind-decorator';
import TodoEditorView from './view/TodoEditorView';
import TodoListStateKeeper from './state/TodoListStateKeeper';
import ReactComponent from 'comp/shared/ReactComponent';


interface Props {
    initialText?: string;
}

interface State {
}

interface InjectedProps {
    todoListStateKeeper: TodoListStateKeeper;
}

@inject('todoListStateKeeper')
@autobind
@observer
class TodoEditorContainer extends ReactComponent<Props, State, InjectedProps> {
    //
    componentDidMount() {
        //
        const { initialText } = this.props;
        const { todoListStateKeeper } = this.injected;

        if (initialText) {
            todoListStateKeeper.changeText(initialText);
        }
    }

    onChangeText(event: React.ChangeEvent<HTMLInputElement>) {
        //
        const { todoListStateKeeper } = this.injected;
        const newText = event.target.value;
        todoListStateKeeper.changeText(newText);
    }

    onClickAddButton() {
        const { todoListStateKeeper } = this.injected;
        todoListStateKeeper.addTodo();
    }

    onRemoveTodo(removeIndex: number) {
        //
        const { todoListStateKeeper } = this.injected;
        todoListStateKeeper.removeTodo(removeIndex);
    }

    moveTop(selectedIndex: number) {
        //
        const { todoListStateKeeper } = this.injected;
        todoListStateKeeper.moveTop(selectedIndex);
    }

    render() {
        //
        const { text, todoList } = this.injected.todoListStateKeeper;

        return (
            <TodoEditorView
                text={text}
                todoList={todoList}
                onChangeText={this.onChangeText}
                onClickAddButton={this.onClickAddButton}
                onRemoveTodo={this.onRemoveTodo}
                onMoveTop={this.moveTop}
            />
        );
    }
}

export default TodoEditorContainer;
