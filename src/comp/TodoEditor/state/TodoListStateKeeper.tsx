import { makeObservable, observable } from "mobx";

class TodoListStateKeeper{

    static instance: TodoListStateKeeper;

    text: string = '';
    todoList: string[] = [];

    constructor(){
        makeObservable(this, {
            text: observable,
            todoList: observable
        })
    }

    changeText(text: string){
        this.text = text;
    }

    addTodo(){
        const newList = [...this.todoList, this.text];
        this.todoList = newList;
        this.text = '';
    }

    removeTodo(idx: number){
        const newList = this.todoList.filter((todo, index)=> index != idx)
        this.todoList = newList;
    }

    moveTop(idx: number){

        const selectedTodo = this.todoList[idx];
        const tempTodoList = this.todoList.filter((todo, index) => index != idx);
        const newList = [selectedTodo, ...tempTodoList];

        this.todoList = newList;
    }

}

TodoListStateKeeper.instance = new TodoListStateKeeper();
export default TodoListStateKeeper;