import React from 'react';
import {TextField, Box, Grid, Button, Typography} from '@material-ui/core';
import TodoListItem from 'comp/TodoListItem';

interface Props{
   text: string;
   todoList: string[];
   onChangeText: (event: React.ChangeEvent<HTMLInputElement>) => void;
   onClickAddButton: () => void;
   onRemoveTodo: (removeIdx: number) => void;
   onMoveTop: (removeIdx: number) => void;
}

class TodoEditorView extends React.Component<Props>{
    
    render(){
        const { text,
                todoList,
                onChangeText,
                onClickAddButton,
                onRemoveTodo,
                onMoveTop
             } = this.props;

        return(
            <Box p ={3}>
                <Grid container>
                    <Grid item xs={8}>
                        <TextField
                            fullWidth
                            value = {text}
                            onChange={onChangeText}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Button onClick={onClickAddButton}>추가</Button>
                    </Grid>
                </Grid>
                <Grid>
                    {
                        todoList.map((todo, index) => (
                            <TodoListItem
                                key = {index} 
                                todo = {todo}
                                index = {index}
                                onRemove = {onRemoveTodo}
                                onMoveTop = {onMoveTop}
                            />
                        ))
                    }
                </Grid>
            </Box>
        )
    }
}

export default TodoEditorView;