import React from 'react';
import {Grid, Button, Typography} from '@material-ui/core';

interface Props{
    todo: string;
    index: number;
    onRemove: (todoIndex: number) => void;
    onMoveTop: (todoIndex: number) => void;
}

class TodoListItemContainer extends React.Component<Props>{
    
    render(){

        const { todo, index, onRemove, onMoveTop } = this.props;

        return(
            <Grid container>
                <Grid item xs={2}>
                <Button onClick ={() => onMoveTop(index)} >TOP</Button>
                </Grid>
                <Grid item xs={8}>
                        <Typography>{todo}</Typography>
                </Grid>
                <Grid item xs={2}>
                        <Button onClick ={() => onRemove(index)} >X</Button>
                </Grid>
            </Grid>
        );

    }
}

export default TodoListItemContainer