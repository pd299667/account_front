import React from 'react';
import autobind from 'autobind-decorator';
import TodoListItem, { Test } from '../comp/TodoListItem';
import TodoEditor from '../comp/TodoEditor';

interface Props{
}

interface State{
}

@autobind
class MainPage extends React.Component<Props, State>{

    render(){
        return (
           <TodoEditor/>
        );
    }

}

export default MainPage;